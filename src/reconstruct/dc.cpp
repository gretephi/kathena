//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file dc.cpp
//  \brief piecewise constant (donor cell) reconstruction

// Athena++ headers
#include "reconstruction.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../mesh/mesh.hpp"
#include "../hydro/hydro.hpp"

//----------------------------------------------------------------------------------------
//! \fn Reconstruction::DonorCellX1()
//  \brief

void Reconstruction::DonorCellX1(MeshBlock *pmb,
  const int kl, const int ku, const int jl, const int ju, const int il, const int iu,
  const AthenaArray<Real> &w_in, const AthenaArray<Real> &bcc_in,
  AthenaArray<Real> &wl_in, AthenaArray<Real> &wr_in) {

  auto w = w_in.get_KView4D();
  auto wl = wl_in.get_KView4D();
  auto wr = wr_in.get_KView4D();

  START_UGLY_4D_LOOP("DonorCell X1",0,NHYDRO-1,kl,ku,jl,ju,il,iu)
        wl(n,k,j,i) = w(n,k,j,i-1);
        wr(n,k,j,i) = w(n,k,j,i  );
  END_UGLY_4D_LOOP

  if (MAGNETIC_FIELDS_ENABLED) {
    auto bcc = bcc_in.get_KView4D();

    START_UGLY_3D_LOOP("DonorCell X1 Mag",kl,ku,jl,ju,il,iu)
        wl(IBY,k,j,i) = bcc(IB2,k,j,i-1);
        wr(IBY,k,j,i) = bcc(IB2,k,j,i  );

        wl(IBZ,k,j,i) = bcc(IB3,k,j,i-1);
        wr(IBZ,k,j,i) = bcc(IB3,k,j,i  );
    END_UGLY_3D_LOOP
  }

  return;
}

//----------------------------------------------------------------------------------------
//! \fn Reconstruction::DonorCellX2()
//  \brief

void Reconstruction::DonorCellX2(MeshBlock *pmb,
  const int kl, const int ku, const int jl, const int ju, const int il, const int iu,
  const AthenaArray<Real> &w_in, const AthenaArray<Real> &bcc_in,
  AthenaArray<Real> &wl_in, AthenaArray<Real> &wr_in) {

  auto w = w_in.get_KView4D();
  auto wl = wl_in.get_KView4D();
  auto wr = wr_in.get_KView4D();

  START_UGLY_4D_LOOP("DonorCell X2",0,NHYDRO-1,kl,ku,jl,ju,il,iu)
        wl(n,k,j,i) = w(n,k,j-1,i);
        wr(n,k,j,i) = w(n,k,j  ,i);
  END_UGLY_4D_LOOP

  if (MAGNETIC_FIELDS_ENABLED) {
    auto bcc = bcc_in.get_KView4D();

    START_UGLY_3D_LOOP("DonorCell X2 Mag",kl,ku,jl,ju,il,iu)
        wl(IBY,k,j,i) = bcc(IB3,k,j-1,i);
        wr(IBY,k,j,i) = bcc(IB3,k,j  ,i);

        wl(IBZ,k,j,i) = bcc(IB1,k,j-1,i);
        wr(IBZ,k,j,i) = bcc(IB1,k,j  ,i);
    END_UGLY_3D_LOOP
  }

  return;
}

//----------------------------------------------------------------------------------------
//! \fn Reconstruction::DonorCellX3()
//  \brief

void Reconstruction::DonorCellX3(MeshBlock *pmb,
  const int kl, const int ku, const int jl, const int ju, const int il, const int iu,
  const AthenaArray<Real> &w_in, const AthenaArray<Real> &bcc_in,
  AthenaArray<Real> &wl_in, AthenaArray<Real> &wr_in) {

  auto w = w_in.get_KView4D();
  auto wl = wl_in.get_KView4D();
  auto wr = wr_in.get_KView4D();

  START_UGLY_4D_LOOP("DonorCell X3",0,NHYDRO-1,kl,ku,jl,ju,il,iu)
        wl(n,k,j,i) = w(n,k-1,j,i);
        wr(n,k,j,i) = w(n,k  ,j,i);
  END_UGLY_4D_LOOP

  if (MAGNETIC_FIELDS_ENABLED) {
    auto bcc = bcc_in.get_KView4D();

    START_UGLY_3D_LOOP("DonorCell X3 Mag",kl,ku,jl,ju,il,iu)
        wl(IBY,k,j,i) = bcc(IB1,k-1,j,i);
        wr(IBY,k,j,i) = bcc(IB1,k  ,j,i);

        wl(IBZ,k,j,i) = bcc(IB2,k-1,j,i);
        wr(IBZ,k,j,i) = bcc(IB2,k  ,j,i);
    END_UGLY_3D_LOOP
  }

  return;
}
