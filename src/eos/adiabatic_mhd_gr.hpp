KOKKOS_INLINE_FUNCTION
Real SoundSpeed(const Real[]) const {return 0.0;}

KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real[], const Real) const {return 0.0;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real, Real, Real, Real, Real *, Real *) const {return;}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &prim,
		const AthenaArray<Real> &bbx_vals, int k, int j, int il, int iu, int ivx,
		AthenaArray<Real> &lambdas_p, AthenaArray<Real> &lambdas_m) const;

KOKKOS_INLINE_FUNCTION
void SoundSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real *, Real *) const
		{return;}

//----------------------------------------------------------------------------------------
// Function for calculating relativistic fast wavespeeds in arbitrary coordinates
// Inputs:
//   rho_h: gas enthalpy
//   pgas: gas pressure
//   u0,u1: contravariant components of 4-velocity
//   b_sq: b_\mu b^\mu
//   g00,g01,g11: contravariant components of metric
// Outputs:
//   plambda_plus: value set to most positive wavespeed
//   plambda_minus: value set to most negative wavespeed
// Notes:
//   follows same general procedure as vchar() in phys.c in Harm
//   variables are named as though 1 is normal direction
KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real rho_h, Real pgas, Real u0, Real u1,
    Real b_sq, Real g00, Real g01, Real g11, Real *plambda_plus, Real *plambda_minus) 
  const{
  // Parameters and constants
  const Real gamma_adi = gamma_;

  // Calculate comoving fast magnetosonic speed
  Real cs_sq = gamma_adi * pgas / rho_h;
  Real va_sq = b_sq / (b_sq + rho_h);
  Real cms_sq = cs_sq + va_sq - cs_sq * va_sq;

  // Set fast magnetosonic speeds in appropriate coordinates
  Real a = SQR(u0) - (g00 + SQR(u0)) * cms_sq;
  Real b = -2.0 * (u0*u1 - (g01 + u0*u1) * cms_sq);
  Real c = SQR(u1) - (g11 + SQR(u1)) * cms_sq;
  Real d = std::max(SQR(b) - 4.0*a*c, 0.0);
  Real d_sqrt = std::sqrt(d);
  Real root_1 = (-b + d_sqrt) / (2.0*a);
  Real root_2 = (-b - d_sqrt) / (2.0*a);
  if (root_1 > root_2) {
    *plambda_plus = root_1;
    *plambda_minus = root_2;
  } else {
    *plambda_plus = root_2;
    *plambda_minus = root_1;
  }
  return;
}
