
//----------------------------------------------------------------------------------------
// \!fn Real EquationOfState::SoundSpeed(Real prim[NHYDRO])
// \brief returns adiabatic sound speed given vector of primitive variables
KOKKOS_INLINE_FUNCTION
Real SoundSpeed(const Real prim[NHYDRO]) const {
  return std::sqrt(gamma_*prim[IPR]/prim[IDN]);
}

KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real[], const Real) const {return 0.0;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real, Real, Real, Real, Real *, Real *) const {return;}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &,
		const AthenaArray<Real> &, int, int, int, int, int, AthenaArray<Real> &,
		AthenaArray<Real> &) const {return;}

KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real,
		Real *, Real *) const {return;}
