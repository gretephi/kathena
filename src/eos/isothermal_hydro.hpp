
KOKKOS_INLINE_FUNCTION
//----------------------------------------------------------------------------------------
// \!fn Real EquationOfState::SoundSpeed(Real dummy_arg[NHYDRO])
// \brief returns isothermal sound speed

Real SoundSpeed(const Real dummy_arg[NHYDRO]) const {
  return iso_sound_speed_;
}

KOKKOS_INLINE_FUNCTION
Real FastMagnetosonicSpeed(const Real[], const Real) {return 0.0;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsSR(Real, Real, Real, Real, Real *, Real *) {return;}

void FastMagnetosonicSpeedsSR(const AthenaArray<Real> &,
		const AthenaArray<Real> &, int, int, int, int, int, AthenaArray<Real> &,
		AthenaArray<Real> &) {return;}

KOKKOS_INLINE_FUNCTION
void SoundSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real *, Real *)
		{return;}

KOKKOS_INLINE_FUNCTION
void FastMagnetosonicSpeedsGR(Real, Real, Real, Real, Real, Real, Real, Real,
		Real *, Real *) {return;}

